package com.whatthefox.minecraft.gamecore.event;

import com.whatthefox.minecraft.gamecore.entity.GameState;
import com.whatthefox.minecraft.gamecore.manager.GameCoreManager;
import com.whatthefox.minecraft.gamecore.manager.MessageManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerInteraction implements Listener {

  @EventHandler
  public void breakBlock(final BlockBreakEvent event) {
    if (!GameCoreManager.isDebug()) {
      event.setCancelled(true);
    }
  }

  @EventHandler
  public void onPlayerJoin(final PlayerJoinEvent event) {
    event.setJoinMessage(null);
    GameCoreManager.addPlayer(event.getPlayer());
  }

  @EventHandler
  public void onPlayerQuit(final PlayerQuitEvent event) {
    event.setQuitMessage(null);
    GameCoreManager.removePlayer(event.getPlayer());
  }

  @EventHandler
  public void onPlayerPreConnect(final AsyncPlayerPreLoginEvent event) {
    if (!GameCoreManager.isDebug()) {
      if (GameCoreManager.getState() == GameState.INIT) {
        event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, MessageManager.getMessage("server-not-ready"));
      }
    }
  }


}
