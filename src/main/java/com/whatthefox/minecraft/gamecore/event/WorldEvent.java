package com.whatthefox.minecraft.gamecore.event;

import com.whatthefox.minecraft.gamecore.manager.GameCoreManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class WorldEvent implements Listener {

  @EventHandler
  public void onWeatherChange(final WeatherChangeEvent event) {
    if (!GameCoreManager.isDebug()) {
      if (event.toWeatherState()) {
        event.setCancelled(true);
      }
    }
  }

  @EventHandler
  public void onThunderChange(final ThunderChangeEvent event) {
    if (!GameCoreManager.isDebug()) {
      if (event.toThunderState()) {
        event.setCancelled(true);
      }
    }
  }
}
