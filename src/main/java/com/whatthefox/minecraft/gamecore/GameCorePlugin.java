package com.whatthefox.minecraft.gamecore;

import com.whatthefox.minecraft.gamecore.cmd.CoreCommand;
import com.whatthefox.minecraft.gamecore.event.PlayerInteraction;
import com.whatthefox.minecraft.gamecore.event.WorldEvent;
import com.whatthefox.minecraft.gamecore.manager.ThreeDLabelManager;
import org.bukkit.plugin.java.JavaPlugin;

public class GameCorePlugin extends JavaPlugin {

  public static GameCorePlugin getInstance() {
    return getPlugin(GameCorePlugin.class);
  }

  @Override
  public void onEnable() {
    // Register events
    getServer().getPluginManager().registerEvents(new PlayerInteraction(), this);
    getServer().getPluginManager().registerEvents(new WorldEvent(), this);
    // Register bungeecord channel
    getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    // Register command
    this.getCommand("core").setExecutor(new CoreCommand());
  }

  @Override
  public void onDisable() {
    ThreeDLabelManager.clean();
  }

}
