package com.whatthefox.minecraft.gamecore.entity;

import org.bukkit.Location;

public record GameManagerInfo(int minPlayer, int maxPlayer, Location lobby, String gameName) {

}
