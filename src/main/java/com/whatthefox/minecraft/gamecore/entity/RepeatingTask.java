package com.whatthefox.minecraft.gamecore.entity;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class RepeatingTask implements Runnable {

  private final int taskId;

  public RepeatingTask(final JavaPlugin plugin, final int initialDelay, final int repeatDelay) {
    this.taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, this, initialDelay, repeatDelay);
  }

  public void cancelLoop() {
    Bukkit.getScheduler().cancelTask(this.taskId);
  }
}