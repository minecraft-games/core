package com.whatthefox.minecraft.gamecore.entity;

import java.util.List;
import org.bukkit.entity.Player;

public interface Observer {

  void startGame(final List<Player> players, final List<Player> spectators);
}
