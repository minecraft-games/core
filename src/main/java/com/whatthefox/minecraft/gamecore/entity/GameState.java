package com.whatthefox.minecraft.gamecore.entity;

public enum GameState {
  INIT,
  WAITING_TO_START,
  STARTED_COUNTDOWN,
  IN_PROGRESS,
  ENDING
}
