package com.whatthefox.minecraft.gamecore.manager;

import com.whatthefox.minecraft.gamecore.GameCorePlugin;
import com.whatthefox.minecraft.gamecore.entity.GameManagerInfo;
import com.whatthefox.minecraft.gamecore.entity.GameState;
import com.whatthefox.minecraft.gamecore.entity.Observer;
import com.whatthefox.minecraft.gamecore.entity.RepeatingTask;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

public class GameCoreManager {

  private static final List<Observer> observers = new ArrayList<>();
  private static List<Player> players = new ArrayList<>();
  private static List<Player> spectators = new ArrayList<>();
  private static GameState state = GameState.INIT;
  private static GameManagerInfo infos = null;
  private static RepeatingTask currentTask;
  private static boolean debug = false;

  public static void init(final GameManagerInfo infos) {
    // Avoid keeping players from previous game
    players = new ArrayList<>();
    spectators = new ArrayList<>();
    // Prepare game manager
    GameCorePlugin.getInstance().getLogger().info("Init GameManager with game name " + infos.gameName());
    MessageManager.setPrefix(infos.gameName());
    GameCoreManager.infos = infos;
    state = GameState.WAITING_TO_START;
    // Add online players to the game (developer purpose)
    Bukkit.getOnlinePlayers().forEach(GameCoreManager::addPlayer);
  }

  public static void addPlayer(final Player player) {
    checkGameInitialization();
    if (GameState.WAITING_TO_START == state || GameState.STARTED_COUNTDOWN == state) {
      if (players.size() >= infos.maxPlayer()) {
        setSpectator(player, true);
      } else {
        GameCorePlugin.getInstance().getLogger().info("Add player " + player.getName() + " to players");
        players.add(player);
        player.setGameMode(GameMode.ADVENTURE);
        MessageManager.sendGlobalChat("join", player.getName(), players.size(), infos.maxPlayer());
        if (players.size() >= infos.minPlayer()) {
          startCountdown();
        }
      }
    } else {
      setSpectator(player, true);
    }
    player.teleport(infos.lobby());
  }

  public static void removePlayer(final Player player) {
    checkGameInitialization();
    if (players.contains(player)) {
      GameCorePlugin.getInstance().getLogger().info("Remove player " + player.getName() + " from players");
      players.remove(player);
      if (players.size() < infos.minPlayer()) {
        stopCountdown();
      }
      MessageManager.sendGlobalChat("leave", player.getName(), players.size(), infos.maxPlayer());
    } else {
      GameCorePlugin.getInstance().getLogger().info("Remove player " + player.getName() + " from spectators");
      spectators.remove(player);
    }
  }

  public static void setSpectator(final Player player, final boolean teleportToLobby) {
    checkGameInitialization();
    GameCorePlugin.getInstance().getLogger().info("Set player " + player.getName() + " to spectator");
    players.remove(player);
    spectators.add(player);
    player.setGameMode(GameMode.SPECTATOR);
    if (teleportToLobby) {
      player.teleport(infos.lobby());
    }
    MessageManager.sendChatMessage(player, "join-spectator", null);
  }

  public static void subscribe(final Observer observer) {
    observers.add(observer);
  }

  private static void startCountdown() {
    GameCorePlugin.getInstance().getLogger().info("Start countdown");
    if (state == GameState.STARTED_COUNTDOWN) {
      GameCorePlugin.getInstance().getLogger().info("Skip start countdown because of current state " + state);
      return;
    }
    state = GameState.STARTED_COUNTDOWN;
    final AtomicInteger timer = new AtomicInteger(10);
    currentTask = new RepeatingTask(GameCorePlugin.getInstance(), 0, 20) {
      @Override
      public void run() {
        if (timer.get() == 0) {
          cancelLoop();
          startGame();
        } else {
          MessageManager.sendGlobalChat("countdown", timer.getAndDecrement());
        }
      }
    };
  }

  private static void stopCountdown() {
    if (state == GameState.STARTED_COUNTDOWN) {
      GameCorePlugin.getInstance().getLogger().info("Stop countdown");
      if (currentTask != null) {
        currentTask.cancelLoop();
      }
      MessageManager.sendGlobalChat("cancel-countdown");
      state = GameState.WAITING_TO_START;
    }
  }

  private static void startGame() {
    GameCorePlugin.getInstance().getLogger().info("Start game > Sending data to " + observers);
    state = GameState.IN_PROGRESS;
    observers.forEach(observer -> observer.startGame(players, spectators));
  }

  public static void endGame() {
    checkGameInitialization();
    if (state == GameState.ENDING) {
      GameCorePlugin.getInstance().getLogger().info("Skip end game because of current state " + state);
      return;
    }
    GameCorePlugin.getInstance().getLogger().info("End game called");
    state = GameState.ENDING;
    players.forEach(player -> player.teleport(infos.lobby()));
    final AtomicInteger timer = new AtomicInteger(10);
    currentTask = new RepeatingTask(GameCorePlugin.getInstance(), 0, 20) {
      @Override
      public void run() {
        if (timer.get() == 0) {
          cancelLoop();
          Bukkit.getOnlinePlayers().forEach(player -> sendPlayerToServer(player, "games"));
        } else {
          timer.getAndDecrement();
        }
      }
    };
  }

  public static GameState getState() {
    return state;
  }

  private static void checkGameInitialization() {
    // Avoid setting player as spectator if game is not ready
    if (infos == null) {
      throw new IllegalStateException("GameManager is not initialized");
    }
  }

  public static boolean isDebug() {
    return debug;
  }

  public static void setDebug(final boolean debug) {
    GameCoreManager.debug = debug;
  }

  public static void sendPlayerToServer(final Player player, final String server) {
    try {
      final ByteArrayOutputStream b = new ByteArrayOutputStream();
      final DataOutputStream out = new DataOutputStream(b);
      out.writeUTF("Connect");
      out.writeUTF(server);
      player.sendPluginMessage(GameCorePlugin.getInstance(), "BungeeCord", b.toByteArray());
      b.close();
      out.close();
    } catch (final Exception e) {
      player.sendMessage(ChatColor.RED + "Error when trying to connect to " + server);
    }
  }
}
