package com.whatthefox.minecraft.gamecore.manager;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;

public class ThreeDLabelManager {

  private static final Map<UUID, ArmorStand> armorStands = new HashMap<>();

  public static UUID createLabel(final Location location, final String name) {
    final ArmorStand armorStand = location.getWorld().spawn(location, ArmorStand.class);
    armorStand.setVisible(false);
    armorStand.setCustomNameVisible(true);
    armorStand.setCustomName(name);
    armorStand.setInvulnerable(true);
    armorStand.setAI(false);
    final var id = UUID.randomUUID();
    armorStands.put(id, armorStand);
    return id;
  }

  public static void deleteLabel(final UUID id) {
    final ArmorStand armorStand = armorStands.get(id);
    if (armorStand != null) {
      armorStand.remove();
      armorStands.remove(id);
    }
  }

  public static void clean() {
    armorStands.values().forEach(ArmorStand::remove);
    armorStands.clear();
  }
}
