package com.whatthefox.minecraft.gamecore.manager;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class MessageManager {

  final static Map<String, String> messages = Map.ofEntries(
      new AbstractMap.SimpleEntry<>("join", "%s a rejoint la partie (%d/%d)"),
      new AbstractMap.SimpleEntry<>("leave", "%s a quitté la partie (%d/%d))"),
      new AbstractMap.SimpleEntry<>("server-not-ready", "Le serveur est en préparation"),
      new AbstractMap.SimpleEntry<>("countdown", "Lancement de la partie dans %d secondes"),
      new AbstractMap.SimpleEntry<>("cancel-countdown", "Annulation du lancement de la partie"),
      new AbstractMap.SimpleEntry<>("end-kick", "Fin de la partie. Merci d'avoir joué"),
      new AbstractMap.SimpleEntry<>("already-started", "La partie a déjà commencé"),
      new AbstractMap.SimpleEntry<>("join-spectator", "Vous avez rejoint les spectateurs de la partie")
  );
  private static String prefix;

  public static void setPrefix(final String prefix) {
    MessageManager.prefix = prefix;
  }

  // Global message

  public static void sendGlobalChat(final String messageId, final Object... args) {
    Bukkit.broadcastMessage(getMessage(messageId, args));
  }

  public static void sendGlobalChat(final String message) {
    Bukkit.broadcastMessage(message);
  }

  public static void sendGlobalBar(final String messageId, final Object... args) {
    sendBarMessage((List<Player>) Bukkit.getOnlinePlayers().stream().toList(), messageId, args);
  }

  public static void sendGlobalBar(final String message) {
    sendBarMessage((List<Player>) Bukkit.getOnlinePlayers().stream().toList(), message);
  }

  // Formated chat message

  public static void sendChatMessage(final Player player, final String messageId, final Object... args) {
    sendMessage(player, getMessage(messageId, args), ChatMessageType.CHAT);
  }

  public static void sendChatMessage(final List<Player> players, final String messageId, final Object... args) {
    players.forEach(player -> sendChatMessage(player, messageId, args));
  }

  // Formated bar message
  public static void sendBarMessage(final Player player, final String messageId, final Object... args) {
    sendMessage(player, getMessage(messageId, args), ChatMessageType.ACTION_BAR);
  }

  public static void sendBarMessage(final List<Player> players, final String messageId, final Object... args) {
    players.forEach(player -> sendBarMessage(player, messageId, args));
  }

  // Chat message

  public static void sendChatMessage(final Player player, final String message) {
    sendMessage(player, message, ChatMessageType.CHAT);
  }

  public static void sendChatMessage(final List<Player> players, final String message) {
    players.forEach(player -> sendChatMessage(player, message));
  }

  // Bar message

  public static void sendBarMessage(final Player player, final String message) {
    sendMessage(player, message, ChatMessageType.ACTION_BAR);
  }

  public static void sendBarMessage(final List<Player> players, final String message) {
    players.forEach(player -> sendBarMessage(player, message));
  }

  // Common

  private static void sendMessage(final Player player, final String message, final ChatMessageType type) {
    player.spigot().sendMessage(type, TextComponent.fromLegacyText(message));
  }

  public static String getMessage(final String messageId, final Object... args) {
    return String.format(messages.get(messageId), args);
  }

}
