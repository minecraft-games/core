package com.whatthefox.minecraft.gamecore.cmd;

import com.whatthefox.minecraft.gamecore.manager.GameCoreManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

public class CoreCommand implements CommandExecutor {

  @Override
  public boolean onCommand(@NotNull final CommandSender commandSender, @NotNull final Command command, @NotNull final String cmd,
      final @NotNull String[] args) {
    if (args.length == 0) {
      commandSender.sendMessage("GameCore plugin");
    } else if (args.length > 0) {
      if (args[0].equalsIgnoreCase("debug")) {
        if (commandSender.isOp()) {
          GameCoreManager.setDebug(!GameCoreManager.isDebug());
          commandSender.sendMessage(ChatColor.GREEN + "Debug mode " + GameCoreManager.isDebug());
        } else {
          commandSender.sendMessage(ChatColor.RED + "You don't have permission to do this");
        }
      } else {
        commandSender.sendMessage("Unknown parameter");
      }
    }
    return false;
  }
}
